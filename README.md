# README

This is an app where users can search for properties available for rent, apply to property listing and pay lease rent to the landlord.
Landlords can manage their properties, listings, leases, collect rent and keep track of expenses and maintenance requests.

Core requirements:
: Anonymous users can search the properties available for rent.
: User must be logged in to manage any part of the platform.

Things you may want to cover:

* Ruby version: 2.6.0p0

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

* About Stripe
The API key you use to authenticate the request determines whether the request is live mode or test mode.

All API requests must be made over HTTPS. Calls made over plain HTTP will fail. API requests without authentication will also fail.

Clients can make requests as connected accounts using the special header Stripe-Account which should contain a Stripe account ID (usually starting with the prefix acct_).

You can use the expand param on any endpoint which returns expandable fields, including list, create, and update endpoints.
