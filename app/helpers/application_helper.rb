module ApplicationHelper
  def full_title(page_title = " ")
    default_title = "Rent Studio™ - Manage Rental Properties"
    if page_title.empty?
      default_title
    else
      "#{page_title} | #{default_title}"
    end
  end

  def time_format(listing_date)
    listing_date.strftime("%A, %d %b %Y %l:%M %p")
  end

  def admins_only(&block)
    block.call if current_user.try(:admin?)
  end

  def avatar_url(user)
    hash = Digest::MD5.hexdigest(user.email.downcase)
    "https://www.gravatar.com/avatar/#{hash}}"
  end

  def property_status(property)
    if property.status == true
      content_tag(:span,"", class: "active")
    else
      content_tag(:span,"", class: "inactive")
    end
  end

  def listing_status(listing)
    if listing.end_date <= Date.today
      content_tag(:span, "", class: "inactive")
    else
      content_tag(:span,"", class: "active")
    end
  end
end
