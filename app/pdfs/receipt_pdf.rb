class ReceiptPdf < Prawn::Document
  def initialize(receipt, view)
    super(top_margin: 70)
    @receipt = receipt
    @view = view
    order_number
    line_items
  end

  def order_number
    text "Receipt \#: #{@receipt.receipt_number}", size: 25, style: :bold
  end

  def line_items
    move_down 20
    text "For property at: #{@receipt.for_lease.for_property.title}"
    move_down 20
    text "Receipt Date: #{@receipt.created_at}"
    move_down 20
    text "Amount: #{format_amount(@receipt.amount)}"
    move_down 20
    text "Currency: #{@receipt.currency_code}"
  end

  def format_amount(num)
    @view.number_to_currency(num)
  end
end
