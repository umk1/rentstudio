class WebhookEventsController < ApplicationController
  #Ignore CSRF (although The POST request is coming from third-party POST request.)
  skip_before_action :verify_authenticity_token
  skip_after_action :verify_authorized

  def create

    # endpoint = Stripe::WebhookEndpoint.create({
    #   url: 'http://localhost:3000/webhook_events/stripe',
    #   enabled_events: [
    #     'charge.failed',
    #     'charge.succeeded',
    #     'checkout.session.completed',
    #   ],
    # })

    #Stripe (optionally) signs the webhook events with signatures.
    #This allows us to verify that the events were sent by Stripe, not by a third party.
    if !valid_signatures?
      render json: { message: "Invalid signatures" }, status: 400
      return
    end

    #if we have already seen this event & it is in database, no need to process it again.
    #idempotent (can be called several times without any issue)
    if !WebhookEvent.find_by(source: params[:source], external_id: external_id).nil?
      render json: {message: "Already processed this web event #{ external_id }"}
      return
    end

    @event = WebhookEvent.create(webhook_params)
    ProcessEventsJob.perform_later(@event.id)
    render json: params #This is the data we are receiving.
  end

  def valid_signatures?

    if params[:source] == "stripe"
      begin
        payload = request.body.read
        sig_header = request.env['HTTP_STRIPE_SIGNATURE']
        endpoint_secret = Rails.application.credentials.dig(:rentstudio_stripe_account, :webhook_secret).to_s

        Stripe::Webhook.construct_event(
          payload, sig_header, endpoint_secret
        )
      rescue JSON::ParserError => e
        # Invalid payload
        return false
      rescue Stripe::SignatureVerificationError => e
        # Invalid signature
        return false
      end
    end
    return true
  end

  private
  def external_id
    return params[:id] if params[:source] == "stripe" # "evt_xxxx"
    SecureRandom.hex # random string if source is not stripe

  end

  def webhook_params
    {
      source: params[:source],
      data: params.except(:source, :action, :controller).permit!,
      external_id: external_id
    }

  end
end
