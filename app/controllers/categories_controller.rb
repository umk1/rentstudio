class CategoriesController < ApplicationController
  before_action :set_category, only: [:show]
  before_action :authenticate_user!, except: [:show]
  skip_after_action :verify_authorized

  def show
    @categories = Category.order(:name)
    @category_properties = @category.properties.order(created_at: :desc).page params[:page]
  end

  private
    def set_category
      @category = Category.friendly.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        flash[:danger] = "The category you requested does not exist."
        redirect_to(request.referrer || properties_url)
    end
end
