class CheckoutController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  skip_after_action :verify_authorized

  def create
    @lease = Lease.find(params[:id]) # This we will make available inside javascript file sent to browser.

    if @lease.nil?  #Failsafe if no lease is found due to invalid params
      redirect_to root_path
      return
    end

    #Update lease details (tenant_id) so that it belongs to current user.
    if @lease.tenant_id.nil? && current_user.email == @lease.tenant_email && @lease.for_property.landlord != current_user #TODO: && current user is not admin
      @lease.update(tenant_id: current_user.id)
      current_user.add_role :tenant if not current_user.has_role? :tenant
    end

    #Now set up a Stripe session for payment
    @session = Stripe::Checkout::Session.create({
        payment_method_types: ['card'],
        line_items: [{
          name: 'Security Deposit for ' + @lease.for_property.title,
          description: 'Paid to ' + @lease.landlord_name,
          amount: (@lease.security_deposit + @lease.pet_deposit) * 100, #stripe wants amount in cents
          currency: 'USD',
          quantity: 1
          },
          {
            name: 'Rent Payment for ' + @lease.for_property.title,
            description: 'Paid to ' + @lease.landlord_name,
            amount: (@lease.rent_amount + @lease.prorated_rent) * 100, #stripe wants amount in cents
            currency: 'USD',
            quantity: 1
            },
        ],
        success_url: checkout_success_url + "?session_id={CHECKOUT_SESSION_ID}&lease_id=#{@lease.id}", #Note: instead of using CHECKOUT_SESSION_ID as a parameter value, better and foolproof method is to use webhooks.
        cancel_url: checkout_cancel_url,
    })

    respond_to do |format|

      format.js # This will render create.js.erb instead of create.html.erb We will then send this javascript file to client browser.
    end
  end

  def success
    #The session_id is returned in params as part of success URL
    #From that param, retrive session details from Stripe end.
    @session = Stripe::Checkout::Session.retrieve(params[:session_id])
    #Now using Stripe session details, retrieve payment intent from Stripe
    @payment_intent = Stripe::PaymentIntent.retrieve(@session.payment_intent)
    @lease = Lease.find(params[:lease_id])

    @receipt = Receipt.new  #(receipt_params)
    @receipt.receipt_number = @lease.generate_receipt_number
    @receipt.amount = @payment_intent.amount_received / 100.00
    @receipt.currency_code = @payment_intent.currency.upcase
    @receipt.lease_id = @lease.id  #Receipt object belongs to Lease
    # #@receipt.status =
    #console
    if @receipt.save
      flash.now[:warning] = "Payment was successful and receipt will be emailed."
      #email receipts
      UserMailer.with(lease: @lease, user: current_user).rent_paid_email.deliver_now
      UserMailer.with(lease: @lease).lease_accepted_by_tenant_email.deliver_now
     else
       flash.now[:warning] = "Payment was successful, but Receipt could not be created."
     end
  end

  def cancel

  end

end
