class PaymentsController < ApplicationController

  skip_after_action :verify_authorized

  def new

    #Step 3.1: Create a PaymentIntent
    payment_intent = Stripe::PaymentIntent.create({
      payment_method_types: ['card'],
      amount: 1000,
      currency: 'usd',
      application_fee_amount: 123,
      transfer_data: {
       destination: 'acct_1HZ1BtFi5NuWGucA',
      }
    })
      #stripe_account: '{{CONNECTED_STRIPE_ACCOUNT_ID}}')
      #stripe_account: 'acct_1HZ1BtFi5NuWGucA' })  #from Rentstudio Dashboard

    console

     #Now we need to read the response from Stripe and get client_secret
     #Use that secret to submit payment

     # In our store builder example, we want to build an experience where customers pay businesses directly. To set this experience up:
     # Indicate a purchase from the business is a direct charge with the "Stripe-Account" header.
     # alternately use "transfer_data" to collect money and then pass to connected account.
     # Either use "transfer_data" or "stripe_account", but not both at the same time.
     # Specify how much of the purchase from the business will go to the platform with application_fee_amount.

     #Included in the returned PaymentIntent is a client secret,
     # which is used on the client side to securely complete the payment process instead of passing the entire PaymentIntent object.
     # You can retrieve the client secret from an endpoint on your server using the browser’s fetch function on the client side.
     # This approach is generally most suitable when your client side is a single-page application
     # get '/secret' do
     #   intent = # ... Create or retrieve the PaymentIntent
     #   {client_secret: intent.client_secret}.to_json
     # end

     # <script>
     # var response = fetch('/secret').then(function(response) {
     #   return response.json();
     # }).then(function(responseJson) {
     #   var clientSecret = responseJson.client_secret;
     #   // Call stripe.confirmCardPayment() with the client secret.
     # });
     # </script>


     #Step 3.2: Collect card details Client-side
     #script.js is different for both approaches. (via platform vs. direct to connected account)

     #Step 3.3: Submit the payment to Stripe Client-side
     #To complete the payment when the user clicks, retrieve the client secret from the PaymentIntent you created in step 3.1
     #and call stripe.confirmCardPayment with the client secret.
     # client.js
     #
     # var form = document.getElementById('payment-form');
     #
     # form.addEventListener('submit', function(ev) {
     #   ev.preventDefault();
     #   stripe.confirmCardPayment(clientSecret, {
     #     payment_method: {
     #       card: card,
     #       billing_details: {
     #         name: 'Jenny Rosen'
     #       }
     #     }
     #   }).then(function(result) {
     #     if (result.error) {
     #       // Show error to your customer (e.g., insufficient funds)
     #       console.log(result.error.message);
     #     } else {
     #       // The payment has been processed!
     #       if (result.paymentIntent.status === 'succeeded') {
     #         // Show a success message to your customer
     #         // There's a risk of the customer closing the window before callback
     #         // execution. Set up a webhook or plugin to listen for the
     #         // payment_intent.succeeded event that handles any business critical
     #         // post-payment actions.
     #       }
     #     }
     #   });
     # });

     # Step 3.4: Fulfillment Server-side

  end


  def pay

  end

  def read_response

  end

  def payment_success

  end

  def payment_error

  end

  def payment_cancel

  end

  def save_payment

  end

end
