class ExpensesController < ApplicationController
  before_action :set_expense, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  skip_after_action :verify_authorized

  def index
    @expenses = Expense.where(property_id: current_user.managed_properties).order(property_id: :asc, payment_date: :desc)
    authorize @expenses, :index?
  end

  def show
    authorize @expense, :show?
  end

  def new
    @expense = Expense.new
    authorize @expense, :new?
  end

  def create
    @expense = Expense.new(expense_params)
    authorize @expense, :create?
    if @expense.save
      flash[:success] = "Expense created successfully."
      redirect_to expenses_path
    else
      flash[:warning] = "Expense not created."
      render "new"
    end
  end

  def edit
    authorize @expense, :edit?
  end

  def update
    authorize @expense, :update?
    if @expense.update(expense_params)
      flash[:success] = "Expense updated."
      redirect_to expenses_path
    else
      flash.now[:warning] = "Expense not updated."
      render "edit"
    end
  end

  def destroy
    authorize @expense, :destroy?
    if @expense.destroy
      flash[:danger] = "Expense deleted successfully."
      redirect_to expenses_path
    else
      flash.now[:warning] = "Expense not deleted."
      render "show"
    end

  end

  private

    def set_expense
      @expense = Expense.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        flash[:danger] = "The page you requested does not exist."
        redirect_to expenses_path
    end

    def expense_params
      params.require(:expense).permit(:title, :payment_date, :expense_amount, :expense_category, :description, :property_id, receipts: [])
    end

end
