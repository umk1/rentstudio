class LeasesController < ApplicationController

  before_action :set_lease, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  skip_after_action :verify_authorized

  def index
    @leases = Lease.where(property_id: current_user.managed_properties)
    authorize @leases, :index?
  end

  def show
    authorize @lease, :show?
  end

  def new
    @lease = Lease.new
    authorize @lease, :new?
  end

  def create
    @lease = Lease.new(lease_params)
    @lease.landlord_name == current_user.full_name #TODO default value not working
    authorize @lease, :create?
    if @lease.save
      current_user.add_role :landlord  if not current_user.has_role?(:landlord)
      UserMailer.with(lease: @lease).lease_ready_for_acceptance_email.deliver_now
      flash[:success] = "Lease created successfully."
      redirect_to leases_path
    else
      flash[:warning] = "Lease not created."
      render "new"
    end
  end

  def edit
    authorize @lease, :edit?
  end

  def update
    authorize @lease, :update?
    if @lease.update(lease_params)
      flash[:success] = "Lease updated."
      redirect_to leases_path
    else
      flash.now[:warning] = "Lease not updated."
      render "edit"
    end
  end

  def destroy
    authorize @lease, :destroy?
    if @lease.destroy
      flash[:danger] = "Lease deleted successfully."
      redirect_to leases_path
    else
      flash[:warning] = "Lease not deleted."
      render leases_path
    end
  end

  private

    def set_lease
      @lease = Lease.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        flash[:danger] = "The page you requested does not exist."
        redirect_to leases_path
    end

    def lease_params
      #ActionController::Parameters.permit_all_parameters = true #To check if this allows all parameters by default. How to do that?
      params.require(:lease).permit(:landlord_name,
          :tenant_name,
          :tenant_email,
          :lease_start_date,
          :lease_end_date,
          :auto_renewal_flag,
          :auto_renewal_days_before,
          :rent_amount,
          :rental_concession,
          :amenities,
          :amenity_fees,
          :application_fee_per_adult,
          :rent_due_date,
          :prorated_rent,
          :prorated_rent_due_date,
          :rent_allowed_in_installments,
          :late_charge_due_date,
          :late_charge_one_time_fee,
          :late_charge_per_day_fee,
          :pet_deposit,
          :pet_fee,
          :pet_rent,
          :security_deposit,
          :emergency_contact_name,
          :emergency_contact_phone,
          :emergency_contact_email,
          :emergency_contact_address1,
          :emergency_contact_address2,
          :emergency_contact_city,
          :emergency_contact_state,
          :emergency_contact_country,
          :additional_terms_and_conditions,
          :property_id
          )
    end
end
