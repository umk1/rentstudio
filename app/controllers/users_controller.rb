class UsersController < ApplicationController
  before_action :set_user, only: [:show]
  skip_after_action :verify_authorized

  def show

  end

  # The following action will create connected account for each landlord.
  def create_connected_account

    account = Stripe::Account.create({
      type: 'standard',
      #type: 'express',
    })

    #Step 2.2: Create an account link
    #Connect Onboarding for Custom Accounts is a web form hosted by Stripe that takes care of collecting identity verification information from users.
    account_links = Stripe::AccountLink.create({
      account: account.id,
      refresh_url: 'http://localhost:3000/reauth', #goes here if the account link is no longer valid. Need to create NEW account link.
      return_url: 'http://localhost:3000/return', #goes here after completion of Connect onboarding flow
      type: 'account_onboarding',
      collect: 'eventually_due', #decided by Stripe Accounts API, requirements hash. e.g. Identity document needed. (Driver's license in USA)
    })

  end

  private
    def set_user
      @user = User.friendly.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        flash[:danger] = "The page you requested does not exist."
        redirect_to (request.referrer || root_url)
    end
end
