class PropertiesController < ApplicationController
  before_action :set_property, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]
  skip_after_action :verify_authorized
  #before_action :authorize_landlord!, only: [:edit, :update, :destroy]

  def index
    if params[:query].present?
      @properties = Property.search params[:query], page: params[:page], per_page: 3
    else
      @properties = Property.order(title: :asc).page params[:page]
    end
    @categories = Category.order(:name)
    authorize @properties, :index?
  end

  def show
    authorize @property, :show?
    @comment = Comment.new
    @comment.property_id = @property.id
    @listing = Listing.where(property_id: @property).last
  end

  def new
    @property = Property.new
    authorize @property, :new?
  end

  def create
   @property = Property.new(property_params)
   authorize @property, :create?
   @property.landlord = current_user

   if @property.save
     flash[:success] = "Property created."
     redirect_to @property
   else
     flash.now[:warning] = "Property not created."
     render "new"
   end
  end

  def edit
    authorize @property, :edit?
  end

  def update
    authorize @property, :update?
    if @property.update(property_params)
      flash[:success] = "Property updated."
      redirect_to @property
    else
      flash.now[:warning] = "Property not updated."
      render "edit"
    end
  end

  def destroy
    authorize @property, :destroy?
    if @property.destroy
      flash[:danger] = "Property deleted successfully."
      redirect_to properties_path
    else
      flash.now[:warning] = "Property not deleted."
      render "show"
    end
  end

  private

    def set_property
      @property = Property.friendly.find(params[:id])
      # authorize @property  # I find it easier to authorize each action separately (show, edit, update, destroy)
      rescue ActiveRecord::RecordNotFound
        flash[:danger] = "The page you requested does not exist."
        redirect_to properties_path
    end

    def property_params
      params.require(:property).permit(:title,:description,:address1,:address2,:city,:state,:postal_code,:country,:category_id,:property_type,:unit_number,:status,:tag_list, :image)
    end

    # following is not needed because now we are using Pundit gem.
    #def authorize_landlord!
    #  authenticate_user!
    #
    #  unless @property.landlord == current_user
    #    flash[:danger] = "You are not authorized to '#{action_name}' the '#{@property.title.upcase}' property."
    #    redirect_to properties_path
    #  end
    #end

  end
