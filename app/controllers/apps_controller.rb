class AppsController < ApplicationController
    before_action :set_app, only: [:show, :edit, :update, :destroy]
    before_action :authenticate_user!
    skip_after_action :verify_authorized #TODO bypass until we fix the policy

    def index

    end

    def show

    end

    def new
      #@listing = params[:id] #TODO how to get listing details for new rental application?
      @app = App.new
      authorize @app, :new?
    end

    def create
      @app = App.new(app_params)
      authorize @app, :create?
      if @app.save
        flash[:success] = "Rental application created successfully."
        redirect_to listings_path
      else
        flash[:warning] = "Rental application not created."
        render "new"
      end
    end

    def edit

    end

    def update

    end

    def destroy

    end

    private
      def set_app
        @app = App.find(params[:id])
        rescue ActiveRecord::RecordNotFound
          flash[:danger] = "The page you requested does not exist."
          redirect_to apps_path
      end

      def app_params
        params.require(:app).permit(
          :move_in_date,
          :lease_term_requested,
          :emergency_contact_name,
          :emergency_contact_phone,
          :emergency_contact_email,
          :emergency_contact_address1,
          :emergency_contact_address2,
          :emergency_contact_city,
          :emergency_contact_state,
          :emergency_contact_country,
          :water_bed_flag,
          :smoker_flag,
          :renters_insurance_flag,
          :military_flag,
          :one_year_military_stay_flag,
          :eviction_flag,
          :asked_to_move_out_flag,
          :ever_breached_lease_flag,
          :bankruptcy_flag,
          :foreclosure_flag,
          :credit_problem_flag,
          :crime_conviction_flag,
          :sex_offender_flag,
          :credit_check_authorization_flag,
          :additional_information,
          :listing_id
        )
      end
end
