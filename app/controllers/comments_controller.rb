class CommentsController < ApplicationController
  before_action :set_property
  before_action :authenticate_user!
  skip_after_action :verify_authorized

  def create
    @comment = @property.comments.create(comment_params)
    @comment.author = current_user
    authorize @comment, :create?

    if @comment.save
      flash[:success] = "Comment created successfully."
      redirect_to property_path(@comment.property)
    else
      flash[:warning] = "Comment not created."
      render "properties/show"
      #redirect_to property_comments_path(@property)
    end
  end

  private
    def set_property
      @property = Property.friendly.find(params[:property_id])
    end

    def comment_params
      params.require(:comment).permit(:body)
    end

end
