class ReceiptsController < ApplicationController

  before_action :set_receipt, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    @receipts = Receipt.all #TODO
    authorize @receipts, :index?
  end

  def show
    authorize @receipt, :show?
    respond_to do |format|
      format.html
      format.pdf do
        pdf = ReceiptPdf.new(@receipt, view_context)  #view_context is passed so as to get access to view helpers (See format_amount in receipt_pdf.rb)
        send_data pdf.render, filename: "#{@receipt.receipt_number}.pdf", type: "application/pdf",
        disposition: "inline"
      end
    end
  end

  # def new
  #   @receipt = Receipt.new
  #   authorize @receipt, :new?
  # end
  #
  # def create
  #
  # end

  def destroy

  end

  def send_by_email

  end

  def download

  end

  private
    def set_receipt
      @lease = Lease.find(params[:lease_id])
      @receipt = Receipt.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        flash[:danger] = "The page you requested does not exist."
        redirect_to leases_path
    end

    def receipt_params
      params.require(:receipt).permit!
    end

end
