class ListingsController < ApplicationController
  before_action :set_listing, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:show]
  skip_after_action :verify_authorized

  def index
    @listings = Listing.where(property_id: current_user.managed_properties)
    authorize @listings, :index?
  end

  def show
    authorize @listing, :show?
  end

  def new
    @listing = Listing.new
    authorize @listing, :new?
  end

  def create
    @listing = Listing.new(listing_params)
    authorize @listing, :create?
    if @listing.save
      flash[:success] = "Listing created successfully."
      redirect_to listings_path
    else
      flash[:warning] = "Listing not created."
      render "new"
    end
  end

  def edit
    authorize @listing, :edit?
  end

  def update
    authorize @listing, :update?
    if @listing.update(listing_params)
      flash[:success] = "Listing updated."
      redirect_to listings_path
    else
      flash.now[:warning] = "Listing not updated."
      render "edit"
    end
  end

  def destroy
    authorize @listing, :destroy?
    if @listing.destroy
      flash[:danger] = "Listing deleted successfully."
      redirect_to listings_path
    else
      flash[:warning] = "Listing not deleted."
      render listings_path
    end
  end

  private

    def set_listing
      @listing = Listing.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        flash[:danger] = "The page you requested does not exist."
        redirect_to listings_path
    end

    def listing_params

      #ActionController::Parameters.permit_all_parameters = true #To check if this allows all parameters by default. How to do that?

      params.require(:listing).permit(:title,
            :description,
            :contact_phone,
            :contact_email,
            :status,
            :rent_amount,
            :security_deposit_amount,
            :other_amount,
            :start_date,
            :end_date,
            :available_date,
            :rental_terms,
            :utilities_paid,
            :smoking_allowed,
            :approval_required,
            :approval_requirements,
            :pets_allowed,
            :pet_deposit_amount,
            :pet_deposit_description,
            :bedroom,
            :full_bath,
            :half_bath,
            :available_area,
            :bedroom_description,
            :room_description,
            :bathroom_description,
            :kitchen_description,
            :complex_access,
            :style,
            :parking,
            :lot_description,
            :furnished,
            :microwave,
            :dishwasher,
            :fireplace,
            :fireplace_description,
            :oven_type,
            :stove_type,
            :washer_dryer,
            :appliances,
            :swimming_pool,
            :pool_description,
            :flooring_description,
            :exterior_description,
            :heating_description,
            :cooling_description,
            :water_description,
            :energy_features,
            :other_amenities,
            :credit_check_required,
            :criminal_background_check_required,
            :credit_check_required,
            :sexual_offender_check_required,
            :eviction_check_required,
            :virtual_tour_link1,
            :virtual_tour_link2,
            :property_id,
            images: []
          )

    end
end
