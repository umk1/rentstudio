class Admin::CategoriesController < Admin::ApplicationController
  before_action :set_category, only: [:edit, :update, :destroy]

  def index
    @categories = Category.order(:name).page params[:page]
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)

    if @category.save
      flash[:success] = "Category created successfully."
      redirect_to admin_categories_path
    else
      flash[:warning] = "Category not created."
      render "new"
    end
  end

  def edit

  end

  def update
    if @category.update(category_params)
      flash[:success] = "Category updated."
      redirect_to admin_categories_path
    else
      flash.now[:warning] = "Category not updated."
      render "edit"
    end
  end

  def destroy
    if @category.destroy
      flash[:danger] = "Category deleted."
      redirect_to admin_categories_path
    else
      flash.now[:warning] = "Category not deleted."
      redirect_to admin_categories_path
    end
  end

  private

    def set_category
      @category = Category.friendly.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        flash[:danger] = "The page you requested does not exist."
        redirect_to admin_categories_path
    end

    def category_params
      params.require(:category).permit(:name, :summary)
    end

end
