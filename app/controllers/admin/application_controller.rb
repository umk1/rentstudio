class Admin::ApplicationController < ApplicationController
  before_action :authorize_admin!
  skip_after_action :verify_authorized

  def index
    @users = User.all
    @categories = Category.all
    @properties = Property.all
    @listings = Listing.all
    @leases = Lease.all
    @tickets = Ticket.all
    @expenses = Expense.all
    @tags = Tag.all
    @comments = Comment.all
    @likes = Like.all
  end

  private

    def authorize_admin!
      authenticate_user!

      unless current_user.admin?
        redirect_to root_path, alert: "You must be an administrator to access"
      end
    end
end
