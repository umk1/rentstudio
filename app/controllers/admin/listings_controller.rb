class Admin::ListingsController < Admin::ApplicationController
  def index
      @listings = Listing.all.page params[:page]
  end
end
