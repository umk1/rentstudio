class Admin::LeasesController < Admin::ApplicationController
  def index
      @leases = Lease.all.page params[:page]
  end
end
