class Admin::ExpensesController < Admin::ApplicationController

  def index
    @expenses = Expense.order(payment_date: :desc).page params[:page]
  end

end
