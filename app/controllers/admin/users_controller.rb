class Admin::UsersController < Admin::ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.order(:email).page params[:page]
  end

  def show

  end

  def new #TODO
    @user = User.new
  end

  def create #TODO

  end

  def edit

  end

  def update
    if @user.update(user_params)
      flash[:success] = "User updated."
      redirect_to admin_users_path
    else
      flash.now[:warning] = "User not updated."
      render "edit"
    end

  end

  def destroy
    if @user.managed_properties.count == 0 && @user.destroy  #delete user only if he is not managing any properties
      flash[:danger] = "User deleted."
      redirect_to admin_users_path
    else
      flash[:warning] = "Landlord User cannot be deleted."
      redirect_to admin_users_path
    end
  end

  private
    def set_user
      @user = User.friendly.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        flash[:danger] = "The page you requested does not exist."
        redirect_to admin_users_path
    end

    def user_params
      params.require(:user).permit(:email, :first_name, :last_name, :username, :organization_name, :admin, role_ids: [])
    end

end
