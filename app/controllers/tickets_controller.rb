class TicketsController < ApplicationController
  before_action :set_ticket, only: [:show]
  before_action :authenticate_user!
  skip_after_action :verify_authorized

  def index
    @tickets = current_user.tickets.order(updated_at: :desc)
    authorize @tickets, :index?
  end

  def show
    authorize @ticket, :show?
  end

  def new
    @ticket = Ticket.new
    authorize @ticket, :new?
  end

  def create
    @ticket = Ticket.create(ticket_params)
    authorize @ticket, :create?
    @ticket.user_id = current_user.id

    if @ticket.save
      flash[:success] = "Ticket created successfully."
      redirect_to tickets_path
    else
      flash[:warning] = "Ticket not created."
      render "new"
    end
  end

  def edit
    authorize @ticket, :edit?
  end

  def update
    authorize @ticket, :edit?
  end

  def destroy
    authorize @ticket, :edit?
  end

  private
    def set_ticket
      @ticket = Ticket.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        flash[:danger] = "The page you requested does not exist."
        redirect_to tickets_path
    end

    def ticket_params
      params.require(:ticket).permit(:user_id, :subject, :ticket_type, :priority, :message, :property_id)
    end
end
