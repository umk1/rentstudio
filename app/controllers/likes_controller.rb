class LikesController < ApplicationController

  before_action :set_property
  before_action :authenticate_user!
  skip_after_action :verify_authorized

  def create
    @like = @property.likes.where(like: params[:like], user_id: current_user).create

    if @like.valid?
      flash[:success] = "You #{params[:like]} this property"
    else
      flash[:danger] =  "You've already made your choice."
    end
    redirect_to property_path(@property)
  end

  private
    def set_property
      @property = Property.friendly.find(params[:property_id])
    end

end
