module Events
  class StripeHandler
    def self.process(event)
      #We want to make this handler work irrespective of whether it is called from controller or not. So we don't want to depend on params.
      #construct an event object based on data in the database.
      stripe_event = Stripe::Event.construct_from(event.data)

      case stripe_event.type
      when 'checkout.session.completed'
        checkout_session = stripe_event.data.object
        sleep(3)
        puts "Update status in database"  #This goes to log
        sleep(3)
        puts "Send email to landlord"     #This goes to log
        sleep(3)
        puts "Send email to renter #{ checkout_session.customer }"       #This goes to log
        #raise "Failed for some reason" #to test for processing error

        # #The session_id is returned in params as part of success URL
        # #From that param, retrive session details from Stripe end.
        # @session = Stripe::Checkout::Session.retrieve(params[:session_id])
        # #Now using Stripe session details, retrieve payment intent from Stripe
        # @payment_intent = Stripe::PaymentIntent.retrieve(@session.payment_intent)

      when 'charge.refunded'

      when 'invoice.created'

      end
    end
  end
end
