class UserMailer < ApplicationMailer

  def lease_ready_for_acceptance_email
    #@user = params[:user]
    @lease = params[:lease]
    @url = lease_url(@lease)
    mail(to: @lease.tenant_email, subject: 'Lease is ready for your acceptance.')
  end

  def lease_accepted_by_tenant_email
    @lease = params[:lease]
    mail(to: @lease.for_property.landlord.email, subject: 'Lease was reviewed and accepted by tenant.')
  end

  def rent_paid_email
    #@lease = params[:lease]
    #@landlord = @lease.landlord_name
    @user = params[:user]
    @url = new_user_session_url
    mail(to: @user.email, subject: 'You have paid the rent.')
  end
end
