class ApplicationMailer < ActionMailer::Base
  default from: 'organio@gmail.com'
  layout 'mailer'
end
