class App < ApplicationRecord
  paginates_per 15
  belongs_to :listing
  #has_one :landlord, through: :property  #TODO will this work?

  validates :emergency_contact_name, presence: true, length: {minimum: 5}
  validates :emergency_contact_phone, presence: true, length: {minimum: 10}
  validates :emergency_contact_email, presence: true, length: {minimum: 5}

end
