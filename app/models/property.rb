class Property < ApplicationRecord
  searchkick
  attr_accessor :tag_list

  extend FriendlyId
  friendly_id :title, use: :slugged
  paginates_per 3

  belongs_to :landlord, class_name: "User", foreign_key: "user_id"
  belongs_to :category
  has_many :comments, dependent: :destroy
  has_many :expenses, class_name: "Expense", dependent: :destroy
  has_many :listings, class_name: "Listing", dependent: :destroy
  has_many :leases, class_name: "Lease", dependent: :destroy
  has_many :apps, through: :listings, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :taggings, dependent: :destroy
  has_many :tags, through: :taggings
  has_many :comments, dependent: :destroy
  has_many :tickets  #we want to keep old tickets even if property is deleted. TODO: Check where to assign such tickets if property is deleted.

  has_one_attached :image

  validates :title, presence: true, length: {minimum: 3}
  validates :description, presence: true, length: {minimum: 5}
  validates :address1, presence: true, length: {minimum: 5}
  validates :postal_code, presence: true
  validates :country, presence: true
  validates :category_id, presence: true
  validates :status, presence: true

  def likes_total
    self.likes.where(like: "like").count
  end

  def dislikes_total
    self.likes.where(like: "dislike").count
  end

  def tag_list #this is getting the tags from database
    tags.join(", ")
  end

  def tag_list=(names) #this is getting the tags from webpage
    tag_names = names.split(",").collect {|str| str.strip.downcase}.uniq
    new_or_existing_tags = tag_names.collect {|tag_name| Tag.find_or_create_by(name: tag_name) }

    self.tags = new_or_existing_tags
  end

end
