class Lease < ApplicationRecord
  paginates_per 15
  resourcify #for rolify gem
  
  #TODO
  #has one or more tenants
  #has one or more occupants
  #rental_concession (All Utilities Included, discounted rent amount, one month of free rent, free parking, waived amenity fees, etc.)
  #amenities (community pool, dry-cleaning pickup, package receiving and dog walking etc.)
  #pet addendum or parking addendum terms

  belongs_to :for_property, class_name: "Property", foreign_key: "property_id"
  belongs_to :tenant, class_name: "User", foreign_key: "tenant_id", optional: true
  has_many :receipts, dependent: :destroy

  validates :landlord_name, presence: true, length: {minimum: 5}
  validates :tenant_name, presence: true, length: {minimum: 5}
  validates :tenant_email, presence: true, length: {minimum: 5}
  validates :lease_start_date, presence: true
  validates :lease_end_date, presence: true
  validates :rent_amount, presence: true
  validates :security_deposit, presence: true

  def generate_receipt_number
    "#{id}_#{tenant_name}_#{Time.now.strftime("%b%Y")}"
  end

end
