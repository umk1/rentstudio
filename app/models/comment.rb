class Comment < ApplicationRecord
  paginates_per 5

  belongs_to :property
  belongs_to :author, class_name: "User"

  validates :body, presence: true, length: {minimum: 3}
end
