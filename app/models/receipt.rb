class Receipt < ApplicationRecord
  belongs_to :for_lease, class_name: "Lease", foreign_key: "lease_id"
end
