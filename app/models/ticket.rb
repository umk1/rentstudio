class Ticket < ApplicationRecord
  belongs_to :user
  belongs_to :property

  validates :subject, presence: true, length: {minimum: 5}
  validates :ticket_type, presence: true
  validates :property_id, presence: true

end
