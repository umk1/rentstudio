class User < ApplicationRecord
  paginates_per 15
  after_create :assign_default_role

  extend FriendlyId
  friendly_id :username, use: :slugged

  has_many :managed_properties, class_name: "Property", dependent: :destroy, inverse_of: 'landlord' #Using :inverse_of option, we can explicitly declare bi-directional associations:
  has_many :comments
  has_many :tickets

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :trackable, :timeoutable, :lockable
  rolify #for rolify gem

  validates :first_name, presence: true, length: {minimum: 3}
  validates :last_name, presence: true, length: {minimum: 3}
  validates :username, presence: true, length: {minimum: 5}
  validate :must_have_a_role, on: :update

  def to_s
    "#{username}"
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def assign_default_role
    self.add_role :applicant if self.roles.blank?
  end

  def must_have_a_role
    unless roles.any?
      errors.add(:roles, "must have at least one role.")
    end
  end

end
