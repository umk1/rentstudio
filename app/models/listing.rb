class Listing < ApplicationRecord
  paginates_per 15

  belongs_to :for_property, class_name: "Property", foreign_key: "property_id"
  has_many :apps, dependent: :destroy
  has_one :landlord, through: :for_property, class_name: "User", foreign_key: "user_id"

  has_many_attached :images

  validates :title, presence: true, length: {minimum: 3}
  validates :rent_amount, presence: true
  validates :security_deposit_amount, presence: true
  validates :available_date, presence: true
  validates :available_area, presence: true
  validates :bedroom, presence: true
  validates :full_bath, presence: true

end
