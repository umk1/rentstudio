class Tag < ApplicationRecord
  extend FriendlyId
  friendly_id :name
  paginates_per 5

  has_many :taggings, dependent: :destroy
  has_many :properties, through: :taggings

  def to_s
    name
  end
end
