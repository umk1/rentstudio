class Category < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged
  paginates_per 3

  validates :name, presence: true, length: { minimum: 5 }
  validates_uniqueness_of :name

  has_many :properties
end
