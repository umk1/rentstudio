class Expense < ApplicationRecord
  paginates_per 15

  belongs_to :for_property, class_name: "Property", foreign_key: "property_id"

  has_many_attached :receipts #TODO receipt model is used for rent receipt.

  validates :title, presence: true, length: {minimum: 3}
  validates :expense_amount, presence: true
  validates :expense_category, presence: true

end
