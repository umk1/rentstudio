class CategoryPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
    user.present? && user.try(:admin?)
  end

  def show?
    user.present? && user.try(:admin?)
  end

  def create?     #policies are the same for create and new actions.
    user.present? && user.try(:admin?) #only admin can create category if he is logged in.
  end

  def new?
    create?
  end

  def update?     #policies are the same for edit and update actions.
    user.present? && user.try(:admin?)
  end

  def edit?
    update?
  end

  def destroy?
    user.present? && user.try(:admin?)
  end

  private
    def category
      record
    end

end
