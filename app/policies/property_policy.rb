class PropertyPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
      #scope.all
    end
  end

  def index?
    true
  end

  def show?
    true
    #scope.where(:id => property.id).exists?
    #record.title[0] == 'V' #scope.where(:title => 'V%').exists?
  end

  def create?     #policies are the same for create and new actions.
    user.present? #any user can create property if he is logged in.
  end

  def new?
    create?
  end

  def update?     #policies are the same for edit and update actions.
   (user.present? && user == property.landlord) || user.try(:admin?)
  end

  def edit?
    update?
  end

  def destroy?
    (user.present? && user == property.landlord) || user.try(:admin?)
  end

  private
    def property
      record
    end

end
