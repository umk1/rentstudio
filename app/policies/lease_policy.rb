class LeasePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
    user.present?
  end

  def show?
    #only landlord or tenant can view the lease.
    user.present? && (( user.email == lease.tenant_email) || ( user.email == lease.for_property.landlord.email )) || user.try(:admin?)
  end

  def create?     #policies are the same for create and new actions.
    ( user.present? && user.managed_properties.count > 0 ) #only landlord can create lease if he is logged in.
  end

  def new?
    create?
  end

  def update?     #policies are the same for edit and update actions.
   (user.present? && user == lease.for_property.landlord) || user.try(:admin?)
  end

  def edit?
    update?
  end

  def destroy?
    (user.present? && user == lease.for_property.landlord) || user.try(:admin?)
  end

  private
    def lease
      record
    end

end
