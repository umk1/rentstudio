class ExpensePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?     #policies are the same for create and new actions.
    user.present? #any user can create property if he is logged in.
  end

  def new?
    create?
  end

  def update?     #policies are the same for edit and update actions.
   (user.present? && user == expense.for_property.landlord) || user.try(:admin?)
  end

  def edit?
    update?
  end

  def destroy?
    (user.present? && user == expense.for_property.landlord) || user.try(:admin?)
  end

  private
    def expense
      record
    end

end
