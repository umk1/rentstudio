class AppPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
    true
    #user == app.property.landlord || user.try(:admin?) #landlord can see list of all applications
  end

  def show?
    true #TODO only renter or the landlord should be able to see the rental application.
  end

  def create?     #policies are the same for create and new actions.
    user.present? #any user can create rental application if he is logged in and TODO: he is not the landlord.
  end

  def new?
    create?
  end

  def update?     #policies are the same for edit and update actions.
   (user.present? && user != app.property.landlord) || user.try(:admin?) #Renter can update application, but landlord cannot.
  end

  def edit?
    update?
  end

  def destroy?
    (user.present? && user != app.property.landlord) || user.try(:admin?) #Renter can delete application, but landlord cannot.
  end

  private
    def app
      record
    end

end
