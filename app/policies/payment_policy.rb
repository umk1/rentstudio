class PaymentPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
    true
  end

  def show?
    true
    #(user == lease.for_property.landlord ) || user.try(:admin?) #TODO only landlord or tenant can view the payment for this lease.
  end

  def create?     #policies are the same for create and new actions.
    ( user.present? ) #&& user.managed_properties.count > 0 ) #only landlord can create lease if he is logged in.
  end

  def new?
    create?
  end

  def update?     #policies are the same for edit and update actions.
   (user.present? && user == lease.for_property.landlord) || user.try(:admin?)
  end

  def edit?
    update?
  end

  def destroy?
    (user.present? && user == lease.for_property.landlord) || user.try(:admin?)
  end

  private
    def payment
      record
    end

end
