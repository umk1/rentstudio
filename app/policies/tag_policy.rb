class TagPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?     #policies are the same for create and new actions.
    (user.present? && user == tag.property.landlord) || user.try(:admin?) #Landlord can create tags if he is logged in.
  end

  def new?
    create?
  end

  def update?     #policies are the same for edit and update actions.
   (user.present? && user == tag.property.landlord) || user.try(:admin?) #Landlord can create tags if he is logged in.
  end

  def edit?
    update?
  end

  def destroy?
    (user.present? && user == tag.property.landlord) || user.try(:admin?) #Landlord can create tags if he is logged in.
  end

  private
    def tag
      record
    end

end
