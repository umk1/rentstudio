class CommentPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def create?
    user.present? #The landlord or admin should be able to give comments/clarifications, so not adding that condition.
  end

end
