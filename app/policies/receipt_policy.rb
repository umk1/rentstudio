class ReceiptPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
    user.present? #TODO only landlord or tenant can view the lease.
  end

  def show?
    user.present?
    #(user == lease.for_property.landlord ) || user.try(:admin?) #TODO only landlord or tenant can view the lease.
  end

  def destroy?
    user.try(:admin?)
  end

  private
    def receipt
      record
    end

end
