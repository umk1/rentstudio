class ListingPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
    true
  end

  def show?
    true
    #(listing.status == true and listing.available_date >= Time.now ) || user.try(:admin?)
  end

  def create?     #policies are the same for create and new actions.
    user.present? #any user can create property if he is logged in.
  end

  def new?
    create?
  end

  def update?     #policies are the same for edit and update actions.
   (user.present? && user == listing.for_property.landlord) || user.try(:admin?)
  end

  def edit?
    update?
  end

  def destroy?
    (user.present? && user == listing.for_property.landlord) || user.try(:admin?)
  end

  private
    def listing
      record
    end

end
