source 'https://rubygems.org'

ruby '2.6.0'

#Rails Default Gems
gem 'rails', '~> 6.0', '>= 6.0.3.4'         # rails framework
gem 'pg', '~> 1.2', '>= 1.2.3'              # database driver
gem 'puma', '~> 3.11'                       # web server
gem 'sass-rails', '~> 5.0'                  # for stylesheets
gem 'uglifier', '>= 1.3.0'                  # compressor for JavaScript assets
gem 'coffee-rails', '~> 5.0.0'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'                    # Build JSON APIs with ease.
gem 'bootsnap', '>= 1.1.0', require: false

# Essential Gems for Project
gem 'awesome_print', '~> 1.8'                     # for rails console model display
#gem 'bootstrap-sass', '~> 3.4', '>= 3.4.1'
gem 'bootstrap', '~> 4.5'                         # bootstrap styling/framework
gem 'jquery-rails', '~> 4.4'
gem 'font-awesome-rails', '~> 4.7', '>= 4.7.0.5'  #for font awesome
gem 'simple_form', '~> 5.0', '>= 5.0.2'           # for form
gem 'country_select', '~> 4.0'
gem 'devise', '~> 4.7', '>= 4.7.2'                # user authentication
gem 'pundit', '~> 2.1'                            # user authorization
gem 'friendly_id', '~> 5.3'                       # create pretty URLs for SEO
gem 'carrierwave', '~> 2.1'                       # uploading files and images
#gem 'fog', '~> 2.2'                               # for cloud deployment of images
gem 'mini_magick', '~> 4.10', '>= 4.10.1'         # manipulate images (carrierwave dependent)
gem 'searchkick', '~> 4.4', '>= 4.4.1'            # Intelligent search made easy with Rails and Elasticsearch
gem 'kaminari', '~> 1.2', '>= 1.2.1'              # for pagination
#gem 'acts-as-taggable-on', '~> 6.5'              # tag a model (TODO)
#gem 'twilio-ruby', '~> 5.39', '>= 5.39.3'        # Send SMS and text, WhatsApp, Chat messages (TODO)
gem 'stripe'
gem 'omniauth', '~> 1.9', '>= 1.9.1'  #TODO
gem 'omniauth-stripe-connect', '~> 2.10', '>= 2.10.1' #TODO
gem 'rolify', '~> 5.3'                          # Assign Roles
gem 'prawn', '~> 2.3'                           # PDF generator

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'better_errors', '~> 2.7', '>= 2.7.1'     #for debugging
  gem 'binding_of_caller', '~> 0.8.0'           #for debugging (used by better_errors, shows live shell)
  gem 'wdm', '>= 0.1.0' if Gem.win_platform?    #avoid polling for changes
  gem 'faker', '~> 2.13'                        #fake data for testing
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem "google-cloud-storage", "~> 1.11", require: false  #TODO remove later
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  gem 'chromedriver-helper'
end

group :production do
  gem "google-cloud-storage", "~> 1.11", require: false
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
