class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.string :subject
      t.string :ticket_type
      t.string :priority
      t.text :message
      t.references :user, foreign_key: true, index: true
      t.references :property, foreign_key: true, index: true

      t.timestamps null: false
    end
  end
end
