class AddTenantEmailToLeases < ActiveRecord::Migration[6.0]
  def change
    add_column :leases, :tenant_email, :string
  end
end
