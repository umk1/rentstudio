class CreateApps < ActiveRecord::Migration[5.2]
  def change
    create_table :apps do |t|
      t.date :move_in_date
      t.string :lease_term_requested
      t.string :emergency_contact_name
      t.string :emergency_contact_phone
      t.string :emergency_contact_email
      t.string :emergency_contact_address1
      t.string :emergency_contact_address2
      t.string :emergency_contact_city
      t.string :emergency_contact_state
      t.string :emergency_contact_country
      t.boolean :water_bed_flag
      t.boolean :smoker_flag
      t.boolean :renters_insurance_flag
      t.boolean :military_flag
      t.boolean :one_year_military_stay_flag
      t.boolean :eviction_flag
      t.boolean :asked_to_move_out_flag
      t.boolean :ever_breached_lease_flag
      t.boolean :bankruptcy_flag
      t.boolean :foreclosure_flag
      t.boolean :credit_problem_flag
      t.boolean :crime_conviction_flag
      t.boolean :sex_offender_flag
      t.boolean :credit_check_authorization_flag
      t.text :additional_information

      t.references :listing, foreign_key: true, index: true

      t.timestamps null: false
    end
  end
end
