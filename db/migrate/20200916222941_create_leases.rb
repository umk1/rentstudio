class CreateLeases < ActiveRecord::Migration[5.2]
  def change
    create_table :leases do |t|

      t.string :landlord_name
      t.string :tenant_name
      t.date :lease_start_date
      t.date :lease_end_date
      t.boolean :auto_renewal_flag
      t.integer :auto_renewal_days_before
      t.integer :rent_amount, precision:10, scale: 2
      t.string :rental_concession
      t.string :amenities
      t.integer :amenity_fees, precision:10, scale: 2
      t.integer :application_fee_per_adult, precision:10, scale: 2
      t.date :rent_due_date
      t.integer :prorated_rent, precision:10, scale: 2
      t.date :prorated_rent_due_date
      t.boolean :rent_allowed_in_installments
      t.date :late_charge_due_date
      t.integer :late_charge_one_time_fee, precision:10, scale: 2
      t.integer :late_charge_per_day_fee, precision:10, scale: 2
      t.integer :pet_deposit, precision:10, scale: 2
      t.integer :pet_fee, precision:10, scale: 2
      t.integer :pet_rent, precision:10, scale: 2
      t.integer :security_deposit, precision:10, scale: 2
      t.string :emergency_contact_name
      t.string :emergency_contact_phone
      t.string :emergency_contact_email
      t.string :emergency_contact_address1
      t.string :emergency_contact_address2
      t.string :emergency_contact_city
      t.string :emergency_contact_state
      t.string :emergency_contact_country
      t.text :additional_terms_and_conditions

      t.timestamps null:false
    end
  end
end
