class CreateReceipts < ActiveRecord::Migration[6.0]
  def change
    create_table :receipts do |t|
      t.string :receipt_number
      t.decimal :amount, precision:10, scale: 2
      t.string :currency_code

      t.timestamps
    end
  end
end
