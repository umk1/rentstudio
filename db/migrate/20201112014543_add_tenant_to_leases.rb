class AddTenantToLeases < ActiveRecord::Migration[6.0]
  def change
    add_reference :leases, :tenant, index: true
    add_foreign_key :leases, :users, column: :tenant_id
  end
end
