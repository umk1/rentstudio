class CreateListings < ActiveRecord::Migration[5.2]
  def change
    create_table :listings do |t|

      t.string :title
      t.text :description
      t.string :contact_phone
      t.string :contact_email
      t.boolean :status
      t.decimal :rent_amount, precision:10, scale: 2
      t.decimal :security_deposit_amount, precision:10, scale: 2
      t.decimal :other_amount, precision: 10, scale: 2
      t.date :start_date
      t.date :end_date
      t.date :available_date
      t.string  :rental_terms
      t.string  :utilities_paid
      t.boolean :smoking_allowed, default: false
      t.boolean :approval_required, default: true
      t.string :approval_requirements
      t.boolean :pets_allowed, default: false
      t.decimal :pet_deposit_amount, precision:10, scale: 2
      t.string :pet_deposit_description
      t.integer :bedroom
      t.integer :full_bath
      t.integer :half_bath
      t.integer :available_area
      t.string :bedroom_description
      t.string :room_description
      t.string :bathroom_description
      t.string :kitchen_description
      t.string :complex_access
      t.string :style
      t.string :parking
      t.string :lot_description
      t.boolean :furnished
      t.boolean :microwave
      t.boolean :dishwasher
      t.integer :fireplace
      t.string :fireplace_description
      t.string :oven_type
      t.string :stove_type
      t.string :washer_dryer
      t.string :appliances
      t.integer :swimming_pool
      t.string :pool_description
      t.string :flooring_description
      t.string :exterior_description
      t.string :heating_description
      t.string :cooling_description
      t.string :water_description
      t.string :energy_features
      t.text :other_amenities
      t.boolean :credit_check_required, default: true
      t.boolean :criminal_background_check_required, default: true
      t.boolean :credit_check_required, default: true
      t.boolean :sexual_offender_check_required, default: true
      t.boolean :eviction_check_required, default: true
      t.string :virtual_tour_link1
      t.string :virtual_tour_link2

      t.timestamps null: false
    end
  end
end
