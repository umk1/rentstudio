class CreateExpenses < ActiveRecord::Migration[5.2]
  def change
    create_table :expenses do |t|
      t.string  :title
      t.date    :payment_date
      t.decimal :expense_amount, precision:10, scale: 2
      t.string  :expense_category
      t.text    :description

      t.timestamps null: false
    end
  end
end
