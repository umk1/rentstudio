class CreateProperties < ActiveRecord::Migration[5.2]
  def change
    create_table :properties do |t|
      t.string  :title
      t.text    :description
      t.string  :address1
      t.string  :address2
      t.string  :city
      t.string  :state
      t.string  :postal_code
      t.string  :country
      t.integer :property_type
      t.string  :unit_number
      t.boolean :status
      t.timestamps null: false
    end
  end
end
