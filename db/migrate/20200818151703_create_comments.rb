class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.text :body
      t.references :property, foreign_key: true, index: true

      t.timestamps null: false
    end
  end
end
