class AddLeaseToReceipt < ActiveRecord::Migration[6.0]
  def change
    add_reference :receipts, :lease, null: false, foreign_key: true
  end
end
