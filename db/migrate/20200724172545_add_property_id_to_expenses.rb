class AddPropertyIdToExpenses < ActiveRecord::Migration[5.2]
  def change
    add_reference :expenses, :property, foreign_key: true
  end
end
