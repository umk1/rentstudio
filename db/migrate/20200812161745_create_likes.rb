class CreateLikes < ActiveRecord::Migration[5.2]
  def change
    create_table :likes do |t|
      t.references :property, foreign_key: true, index: true
      t.references :user, foreign_key: true, index: true
      t.string :like

      t.timestamps null: false
    end
  end
end
