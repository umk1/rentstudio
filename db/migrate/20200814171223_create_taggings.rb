class CreateTaggings < ActiveRecord::Migration[5.2]
  def change
    create_table :taggings do |t|
      t.references :property, foreign_key: true, index: true
      t.references :tag, foreign_key: true, index: true

      t.timestamps null: false
    end
  end
end
