class AddPropertyIdToListings < ActiveRecord::Migration[5.2]
  def change
    add_reference :listings, :property, foreign_key: true
  end
end
