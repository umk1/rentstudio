# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_11_18_215407) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "apps", force: :cascade do |t|
    t.date "move_in_date"
    t.string "lease_term_requested"
    t.string "emergency_contact_name"
    t.string "emergency_contact_phone"
    t.string "emergency_contact_email"
    t.string "emergency_contact_address1"
    t.string "emergency_contact_address2"
    t.string "emergency_contact_city"
    t.string "emergency_contact_state"
    t.string "emergency_contact_country"
    t.boolean "water_bed_flag"
    t.boolean "smoker_flag"
    t.boolean "renters_insurance_flag"
    t.boolean "military_flag"
    t.boolean "one_year_military_stay_flag"
    t.boolean "eviction_flag"
    t.boolean "asked_to_move_out_flag"
    t.boolean "ever_breached_lease_flag"
    t.boolean "bankruptcy_flag"
    t.boolean "foreclosure_flag"
    t.boolean "credit_problem_flag"
    t.boolean "crime_conviction_flag"
    t.boolean "sex_offender_flag"
    t.boolean "credit_check_authorization_flag"
    t.text "additional_information"
    t.bigint "listing_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["listing_id"], name: "index_apps_on_listing_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.string "summary"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.index ["slug"], name: "index_categories_on_slug", unique: true
  end

  create_table "comments", force: :cascade do |t|
    t.text "body"
    t.bigint "property_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "author_id"
    t.index ["author_id"], name: "index_comments_on_author_id"
    t.index ["property_id"], name: "index_comments_on_property_id"
  end

  create_table "expenses", force: :cascade do |t|
    t.string "title"
    t.date "payment_date"
    t.decimal "expense_amount", precision: 10, scale: 2
    t.string "expense_category"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "property_id"
    t.index ["property_id"], name: "index_expenses_on_property_id"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "leases", force: :cascade do |t|
    t.string "landlord_name"
    t.string "tenant_name"
    t.date "lease_start_date"
    t.date "lease_end_date"
    t.boolean "auto_renewal_flag"
    t.integer "auto_renewal_days_before"
    t.integer "rent_amount"
    t.string "rental_concession"
    t.string "amenities"
    t.integer "amenity_fees"
    t.integer "application_fee_per_adult"
    t.date "rent_due_date"
    t.integer "prorated_rent"
    t.date "prorated_rent_due_date"
    t.boolean "rent_allowed_in_installments"
    t.date "late_charge_due_date"
    t.integer "late_charge_one_time_fee"
    t.integer "late_charge_per_day_fee"
    t.integer "pet_deposit"
    t.integer "pet_fee"
    t.integer "pet_rent"
    t.integer "security_deposit"
    t.string "emergency_contact_name"
    t.string "emergency_contact_phone"
    t.string "emergency_contact_email"
    t.string "emergency_contact_address1"
    t.string "emergency_contact_address2"
    t.string "emergency_contact_city"
    t.string "emergency_contact_state"
    t.string "emergency_contact_country"
    t.text "additional_terms_and_conditions"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "property_id"
    t.string "tenant_email"
    t.bigint "tenant_id"
    t.index ["property_id"], name: "index_leases_on_property_id"
    t.index ["tenant_id"], name: "index_leases_on_tenant_id"
  end

  create_table "likes", force: :cascade do |t|
    t.bigint "property_id"
    t.bigint "user_id"
    t.string "like"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["property_id"], name: "index_likes_on_property_id"
    t.index ["user_id"], name: "index_likes_on_user_id"
  end

  create_table "listings", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.string "contact_phone"
    t.string "contact_email"
    t.boolean "status"
    t.decimal "rent_amount", precision: 10, scale: 2
    t.decimal "security_deposit_amount", precision: 10, scale: 2
    t.decimal "other_amount", precision: 10, scale: 2
    t.date "start_date"
    t.date "end_date"
    t.date "available_date"
    t.string "rental_terms"
    t.string "utilities_paid"
    t.boolean "smoking_allowed", default: false
    t.boolean "approval_required", default: true
    t.string "approval_requirements"
    t.boolean "pets_allowed", default: false
    t.decimal "pet_deposit_amount", precision: 10, scale: 2
    t.string "pet_deposit_description"
    t.integer "bedroom"
    t.integer "full_bath"
    t.integer "half_bath"
    t.integer "available_area"
    t.string "bedroom_description"
    t.string "room_description"
    t.string "bathroom_description"
    t.string "kitchen_description"
    t.string "complex_access"
    t.string "style"
    t.string "parking"
    t.string "lot_description"
    t.boolean "furnished"
    t.boolean "microwave"
    t.boolean "dishwasher"
    t.integer "fireplace"
    t.string "fireplace_description"
    t.string "oven_type"
    t.string "stove_type"
    t.string "washer_dryer"
    t.string "appliances"
    t.integer "swimming_pool"
    t.string "pool_description"
    t.string "flooring_description"
    t.string "exterior_description"
    t.string "heating_description"
    t.string "cooling_description"
    t.string "water_description"
    t.string "energy_features"
    t.text "other_amenities"
    t.boolean "credit_check_required", default: true
    t.boolean "criminal_background_check_required", default: true
    t.boolean "sexual_offender_check_required", default: true
    t.boolean "eviction_check_required", default: true
    t.string "virtual_tour_link1"
    t.string "virtual_tour_link2"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "property_id"
    t.index ["property_id"], name: "index_listings_on_property_id"
  end

  create_table "properties", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.string "address1"
    t.string "address2"
    t.string "city"
    t.string "state"
    t.string "postal_code"
    t.string "country"
    t.integer "property_type"
    t.string "unit_number"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.string "slug"
    t.bigint "category_id"
    t.index ["category_id"], name: "index_properties_on_category_id"
    t.index ["slug"], name: "index_properties_on_slug", unique: true
    t.index ["user_id"], name: "index_properties_on_user_id"
  end

  create_table "receipts", force: :cascade do |t|
    t.string "receipt_number"
    t.decimal "amount", precision: 10, scale: 2
    t.string "currency_code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "lease_id", null: false
    t.index ["lease_id"], name: "index_receipts_on_lease_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "taggings", force: :cascade do |t|
    t.bigint "property_id"
    t.bigint "tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["property_id"], name: "index_taggings_on_property_id"
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tickets", force: :cascade do |t|
    t.string "subject"
    t.string "ticket_type"
    t.string "priority"
    t.text "message"
    t.bigint "user_id"
    t.bigint "property_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["property_id"], name: "index_tickets_on_property_id"
    t.index ["user_id"], name: "index_tickets_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "username"
    t.string "organization_name"
    t.boolean "admin", default: false
    t.string "slug"
    t.string "uid"
    t.string "provider"
    t.string "access_code"
    t.string "publishable_key"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["slug"], name: "index_users_on_slug", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  create_table "webhook_events", force: :cascade do |t|
    t.string "source"
    t.string "external_id"
    t.json "data"
    t.integer "state", default: 0
    t.text "processing_errors"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["external_id"], name: "index_webhook_events_on_external_id"
    t.index ["source", "external_id"], name: "index_webhook_events_on_source_and_external_id"
    t.index ["source"], name: "index_webhook_events_on_source"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "apps", "listings"
  add_foreign_key "comments", "properties"
  add_foreign_key "comments", "users", column: "author_id"
  add_foreign_key "expenses", "properties"
  add_foreign_key "leases", "properties"
  add_foreign_key "leases", "users", column: "tenant_id"
  add_foreign_key "likes", "properties"
  add_foreign_key "likes", "users"
  add_foreign_key "listings", "properties"
  add_foreign_key "properties", "categories"
  add_foreign_key "properties", "users"
  add_foreign_key "receipts", "leases"
  add_foreign_key "taggings", "properties"
  add_foreign_key "taggings", "tags"
  add_foreign_key "tickets", "properties"
  add_foreign_key "tickets", "users"
end
