# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# Category.create(
#   name: 'Acerage',
#   summary: 'Acerage'
# )
# Category.create(
#   name: 'Apartments',
#   summary: 'Apartment Complex'
# )
# Category.create(
#   name: 'Condominium',
#   summary: 'Condominium'
# )
# Category.create(
#   name: 'Country Home',
#   summary: 'Country Home'
# )
# Category.create(
#   name: 'Multi Family',
#   summary: 'Duplex/Triplex etc.'
# )
# Category.create(
#   name: 'Single Family',
#   summary: 'Single Family'
# )
# Category.create(
#   name: 'Townhouse/Condo',
#   summary: 'Townhouse/Condo'
# )

# users = 5.times do
#   User.create([email: Faker::Internet.email,
#     password: 'vikhroli',
#     first_name: Faker::Name.first_name,
#     last_name: Faker::Name.last_name,
#     username: Faker::Internet.unique.username])
# end

properties = 5.times do
  Property.create(title: Faker::Address.community,
    description: Faker::Address.full_address,
    address1: Faker::Address.street_address,
    address2: Faker::Address.secondary_address,
    city: Faker::Address.city,
    state: Faker::Address.state_abbr,
    postal_code: Faker::Address.postcode,
    country: "US",
    unit_number: Faker::Address.building_number,
    category_id: Faker::Number.between(from: 1, to: 7),
    user_id: Faker::Number.between(from: 1, to: 10),
    status: Faker::Boolean.boolean)
end
