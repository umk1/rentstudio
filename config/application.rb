require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Rentstudio
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    #config.gmail = config_for(:gmail) # this line loads the config/gmail.yml file and store it in this namespace

    #limit log file to 50MB size, max 3 files retained
    config.logger = ActiveSupport::Logger.new(config.paths['log'].first, 3, 50 * 1024 * 1024)
  end
end
