Rails.configuration.stripe = {
  publishable_key: Rails.application.credentials.dig(:rentstudio_stripe_account, :publishable_key),
  secret_key: Rails.application.credentials.dig(:rentstudio_stripe_account, :secret_key)
}

Stripe.api_key = Rails.application.credentials.dig(:rentstudio_stripe_account, :secret_key)
