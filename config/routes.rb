Rails.application.routes.draw do
  devise_for :users
  root 'home#index'

  scope '/checkout' do
    post 'create', to: 'checkout#create', as: 'checkout_create'
    get 'cancel', to: 'checkout#cancel', as: 'checkout_cancel'
    get 'success', to: 'checkout#success', as: 'checkout_success'
  end

  post '/webhook_events/:source', to: 'webhook_events#create'
  #source could be stripe or github or twilio or facebook etc.

  namespace :admin do
    root 'application#index'
    resources :users
    resources :categories, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :expenses, only: [:index]
    resources :listings, only: [:index]
    resources :apps, only: [:index]
    resources :leases, only: [:index]
    resources :tickets, only: [:index]
  end

  resources :users, only: [:show]
  resources :categories, only: [:show]
  resources :tags, only: [:show]
  resources :expenses
  resources :tickets, only: [:index, :show, :new, :create]

  resources :properties do
    #resources :expenses, only: [:show, :new, :create, :edit, :update, :destroy]
    resources :comments, only: [:create]
    resources :likes, only: [:create]
  end

  resources :listings do
  end

  resources :apps

  resources :leases do
    resources :payments
    resources :receipts, only: [:index, :show, :destroy]
  end

end
